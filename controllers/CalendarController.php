<?php

namespace app\controllers;

use Yii;
use app\models\Calendar;
use app\models\CalendarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\forms\CalendarCopyDay;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CalendarController implements the CRUD actions for Calendar model.
 */
class CalendarController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'remove-day' => ['post'],
                    'copy-day-validate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Calendar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalendarSearch();
        $searchModel->load(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Calendar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Calendar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->get('returnUrl', ['index']));
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Calendar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->get('returnUrl', ['index']));
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Calendar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->get('returnUrl', ['index']));
    }

    /**
     * @param $from
     * @return mixed
     */
    public function actionCopyDay($from)
    {
        $model = new CalendarCopyDay;
        $model->from = $from;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $calendars = Calendar::findAll(['date' => $model->from]);
            foreach($calendars as $calendar) {
                $object = new Calendar($calendar->getAttributes($calendar->activeAttributes()));
                $object->date = $model->to;
                $object->save(false);
            }
            return $this->redirect(['index', get_class($model) => ['date' => $model->to]]);
        } else {
            return $this->renderAjax('copy-day', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function actionCopyDayValidate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new CalendarCopyDay;
        $model->load(Yii::$app->request->post());
        return ActiveForm::validate($model);
    }

    public function actionFulfilled($id)
    {
        $model = $this->findModel($id);
        $model->status = Calendar::STATUS_FULFILLED;
        $model->save(false, ['status']);
        return $this->redirect(['index', 'CalendarSearch' => ['date' => $model->date]]);
    }

    /**
     * @param $from
     * @return mixed
     */
    public function actionRemoveDay($from)
    {
        Calendar::deleteAll(['date' => $from]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Calendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calendar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
