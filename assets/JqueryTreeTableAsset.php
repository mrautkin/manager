<?php

namespace app\assets;

use yii\web\AssetBundle;

class JqueryTreeTableAsset extends AssetBundle
{

    public $sourcePath = '@bower/jquery-treetable';
    public $js = [
        'jquery.treetable.js',
    ];
    public $css = [
        'css/jquery.treetable.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}