<?php

namespace app\models;

use DateTime;
use DateInterval;
use Yii;

/**
 * This is the model class for table "categories".
 *
 */
class PurchaseQuery extends \yii\db\ActiveQuery
{

    /**
     * Выборка за текущую неделю
     *
     * @param string $date Дата для выборки недели
     * @return static
     */
    public function week($date = 'now')
    {
        $date = new DateTime($date);
        $day = $date->format('w')? : 7;
        if( $day = $day - 1 )
            $date->sub(new DateInterval('P' . $day . 'D'));

        $dateBegin = $date->format('Y-m-d');

        $date->add(new DateInterval('P6D'));
        $dateEnd = $date->format('Y-m-d');

        return $this->where(['between', 'date', $dateBegin, $dateEnd]);
    }

    /**
     * Выборка за текущий месяц
     *
     * @param string $date Дата для выборки месяца
     * @return static
     */
    public function month($date = 'now')
    {
        $date = new DateTime($date);
        $numDay = $date->format('t');
        return $this->where(['between', 'date', $date->format('Y-m-01'), $date->format('Y-m-' . $numDay)]);
    }

    /**
     * Выборка за текущий год
     *
     * @param string $date Дата для выборки года
     * @return static
     */
    public function year($date = 'now')
    {
        $date = new DateTime($date);
        return $this->where(['between', 'date', $date->format('Y-01-01'), $date->format('Y-12-31')]);
    }

}
