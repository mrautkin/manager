<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "units_measure".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_title
 */
class UnitsMeasure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'units_measure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_title'], 'required'],
            [['title'], 'string', 'max' => 100],
            [['short_title'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'short_title' => 'Короткое название',
        ];
    }
}
