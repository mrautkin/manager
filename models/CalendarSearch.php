<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CalendarSearch represents the model behind the search form about `app\models\Calendar`.
 */
class CalendarSearch extends Model
{

    /**
     * @var string
     */
    private $_date;

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->_date? : $this->_date = date('Y-m-d');
    }

    /**
     * @param $value
     */
    public function setDate($value)
    {
        $this->_date = $value;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['date', 'required'],
            ['date', 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Calendar::find()
            ->orderBy(['date' => SORT_ASC, 'start_time' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'date' => $this->date,
        ]);

        return $dataProvider;
    }

    public function isDelete()
    {
        return !$this->hasErrors() && true;
    }
}
