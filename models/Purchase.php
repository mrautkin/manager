<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $price
 * @property integer $number
 * @property string $date
 *
 * @property Product $product
 * @property UnitsMeasure $unitsMeasure
 */
class Purchase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'number', 'date', 'price', 'units_measure_id'], 'required'],
            [['product_id', 'units_measure_id'], 'integer'],
            [['price', 'number'], 'number'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Товар',
            'price' => 'Сумма покупки',
            'number' => 'Количество',
            'units_measure_id' => 'Ед.из.',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitsMeasure()
    {
        return $this->hasOne(UnitsMeasure::className(), ['id' => 'units_measure_id']);
    }

    public static function find()
    {
        return new PurchaseQuery(get_called_class());
    }

}
