<?php

namespace app\models;

use Yii;
use yii\base\InvalidValueException;

/**
 * This is the model class for table "calendar".
 *
 * @property integer $id
 * @property string $model_name
 * @property integer $model_id
 * @property string $date
 * @property string $start_time
 * @property string $end_time
 * @property string $status
 */
class Calendar extends \yii\db\ActiveRecord
{

    /**
     * Объект выполнен
     */
    const STATUS_FULFILLED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_name', 'model_id', 'date', 'start_time', 'end_time'], 'required'],
            [['model_id'], 'integer'],
            [['date', 'start_time', 'end_time'], 'safe'],
            [['model_name'], 'in', 'range' => ['Task']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_name' => 'Тип',
            'model_id' => 'Название',
            'date' => 'Дата',
            'start_time' => 'Время начала',
            'end_time' => 'Время окончания',
        ];
    }

    /**
     * @param string $modelClass
     * @return \yii\db\ActiveQuery
     */
    public function getTarget($modelClass = null)
    {
        $items = [
            'Task' => Task::className()
        ];
        $modelClass = $modelClass? : $this->model_name;
        if( array_key_exists($modelClass, $items) ) {
            $class = $items[$modelClass];
        } else {
            throw new InvalidValueException('');
        }
        return $this->hasOne($class, ['id' => 'model_id']);
    }

    public function isFulfilled()
    {
        return $this->status == static::STATUS_FULFILLED;
    }

}
