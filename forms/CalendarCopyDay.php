<?php

namespace app\forms;

use app\models\Calendar;

class CalendarCopyDay extends \yii\base\Model
{

    /**
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $to;

    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'date', 'format' => 'php:Y-m-d'],
            ['to', 'compare', 'compareAttribute' => 'from', 'operator' => '>', 'enableClientValidation' => false],
            ['to', 'validatorToDate'],
        ];
    }

    public function validatorToDate($attribute, $params)
    {
        if( Calendar::find()->where(['date' => $this->to])->exists() )
            $this->addError($attribute, 'Для этой даты уже есть данные');
    }

}