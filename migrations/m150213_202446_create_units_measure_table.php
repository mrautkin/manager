<?php

use yii\db\Schema;
use yii\db\Migration;

class m150213_202446_create_units_measure_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('units_measure', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
            'short_title' => Schema::TYPE_STRING . '(20) NOT NULL',
        ]);
        return true;
    }

    public function safeDown()
    {
        $this->dropTable('units_measure');
        return true;
    }

}
