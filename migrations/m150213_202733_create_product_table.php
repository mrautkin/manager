<?php

use yii\db\Schema;
use yii\db\Migration;

class m150213_202733_create_product_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('product', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
        ]);
        return true;
    }

    public function safeDown()
    {
        $this->dropTable('product');
        return true;
    }

}
