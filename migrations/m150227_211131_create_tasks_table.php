<?php

use yii\db\Schema;
use yii\db\Migration;

class m150227_211131_create_tasks_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('tasks', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'status' => Schema::TYPE_STRING . '(30)',
        ]);
        return true;
    }

    public function safeDown()
    {
        $this->dropTable('tasks');
        return true;
    }
}
