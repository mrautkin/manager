<?php

use yii\db\Schema;
use yii\db\Migration;

class m150227_211146_create_calendar_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('calendar', [
            'id' => Schema::TYPE_PK,
            'model_name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'date' => Schema::TYPE_DATE . ' NOT NULL',
            'start_time' => Schema::TYPE_TIME . ' NOT NULL',
            'end_time' => Schema::TYPE_TIME . ' NOT NULL',
            'status' => Schema::TYPE_STRING . '(30)',
        ]);
        return true;
    }

    public function safeDown()
    {
        $this->dropTable('calendar');
        return true;
    }
}
