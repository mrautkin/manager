<?php

use yii\db\Schema;
use yii\db\Migration;

class m150213_202746_create_purchase_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('purchase', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'units_measure_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'price' => Schema::TYPE_DECIMAL . '(10, 2) NOT NULL DEFAULT 0.00',
            'number' => Schema::TYPE_DECIMAL . '(10, 4) NOT NULL',
            'date' => Schema::TYPE_DATE . ' NOT NULL',
        ]);
        $this->createIndex('i_product_id', 'purchase', 'product_id');
        $this->addForeignKey('fk_purchase_product_id', 'purchase', 'product_id', 'product', 'id', 'RESTRICT');
        $this->createIndex('i_units_measure_id', 'purchase', 'units_measure_id');
        $this->addForeignKey('fk_purchase_units_measure_id', 'purchase', 'units_measure_id', 'units_measure', 'id', 'RESTRICT');
        return true;
    }

    public function safeDown()
    {
        $this->dropTable('purchase');
        return true;
    }

}
