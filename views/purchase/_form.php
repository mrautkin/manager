<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use vova07\select2\Widget as Select2;
use \app\models\Product;
use \app\models\UnitsMeasure;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $form yii\widgets\ActiveForm */
if( !$model->date )
    $model->date = date('Y-m-d');
?>

<div class="purchase-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-4',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'product_id')->widget(Select2::className(), [
        'options' => [
            'prompt' => '',
        ],
        'settings' => [
            'width' => '100%',
            'placeholder' => 'Выберите товар',
            'allowClear' => true
        ],
        'items' => ArrayHelper::map(Product::find()->all(), 'id', 'title')
    ]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => 10]) ?>

    <?= $form
        ->field($model, 'units_measure_id')
        ->dropDownList(ArrayHelper::map(UnitsMeasure::find()->all(), 'id', 'short_title'), ['prompt' => '-Выберите едирицу иземерения-']) ?>

    <?= $form
        ->field($model, 'date', ['horizontalCssClasses' => ['wrapper' => 'col-sm-3']])
        ->widget(DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            //'options' => ['class' => 'form-control'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) ?>

    <div class="form-group">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
