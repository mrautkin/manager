<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Purchase;
use yii\grid\DataColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Purchases';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="purchase-index">

    <div class="row">
        <div class="col-xs-8">
            <h1><?= Html::encode($this->title) ?></h1>
            <p>
                <?= Html::a('Create Purchase', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="col-xs-4">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td nowrap>Сумма за год</td>
                        <td nowrap>Сумма за месяц</td>
                        <td nowrap>Сумма за неделю</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center"><?= Purchase::find()->year()->sum('price'); ?></td>
                        <td align="center"><?= Purchase::find()->month()->sum('price'); ?></td>
                        <td align="center"><?= Purchase::find()->week()->sum('price'); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'date',
                'label' => 'Дата',
                'content' => function(Purchase $model){
                        return Yii::$app->getFormatter()->asDate($model->date, 'd MMMM y');
                    }
            ],
            [
                'attribute' => 'product_id',
                'label' => 'Название товара',
                'content' => function(Purchase $model){
                        return $model->product->title;
                    }
            ],
            'price',
            [
                'class' => DataColumn::className(),
                'value' => function(Purchase $model){
                        return strtr('{number} ({unit})', [
                            '{number}' => rtrim(rtrim((string) $model->number, '0'), '.'),
                            '{unit}' => $model->unitsMeasure->short_title,
                        ]);
                    }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
