<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\time\TimePicker;
use kartik\date\DatePicker;
use vova07\select2\Widget as Select2;
use \app\models\Task;

/* @var $this yii\web\View */
/* @var $model app\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'model_name')->dropDownList([
                'Task' => 'Задача',
            ]) ?>
        </div>
        <div class="col-sm-8">
            <?= $form->field($model, 'model_id')->widget(Select2::className(), [
                'options' => [
                    'prompt' => '',
                ],
                'settings' => [
                    'width' => '100%',
                    'placeholder' => 'Выберите задачу',
                    'allowClear' => true
                ],
                'items' => ArrayHelper::map(Task::find()->all(), 'id', 'title')
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'date')->label(false)->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_INPUT,
                'options' => [
                    'placeholder' => $model->getAttributeLabel('date')
                ],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ],
            ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form
                ->field($model, 'start_time')
                ->label(false)
                ->widget(TimePicker::className(), $timeOptions = [
                    'options' => ['placeholder' => $model->getAttributeLabel('start_time')],
                    'pluginOptions' => [
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        'showInputs' => false,
                        'disableFocus' => true,
                        'defaultTime' => false
                    ]
                ]); ?>
        </div>
        <div class="col-sm-4">
            <?php $timeOptions['options']['placeholder'] = $model->getAttributeLabel('end_time'); ?>
            <?= $form
                ->field($model, 'end_time')
                ->label(false)
                ->widget(TimePicker::className(), $timeOptions) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
