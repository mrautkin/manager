<?php

use yii\helpers\Html;
use app\models\Calendar;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalendarSearch */

$formatter = Yii::$app->getFormatter();
$interval = new DateInterval('P1D');
$dateCurrent = new DateTime();
$dateActive = new DateTime($searchModel->date);

$dateMonthBegin = $dateActive->format('Y-m-01');
$dateMonthEnd = $dateActive->format('Y-m-' . $dateActive->format('t'));

$date = new DateTime($dateMonthBegin);
$day = $date->format('w')? : 7;
if( $day = $day - 1 )
    $date->sub(new DateInterval('P' . $day . 'D'));

$datePrev = new DateTime($dateMonthBegin);
$datePrev->sub($interval);

$dateNext = new DateTime($dateMonthEnd);
$dateNext->add($interval);
?>

<div class="row">
    <div class="col-sm-2">
        <?= Html::a(
            '<span class="glyphicon glyphicon-prev"></span> Предыдущий',
            ['index', 'CalendarSearch' => ['date' => $datePrev->format('Y-m-d')]], [
                'class' => 'btn btn-primary btn-xs btn-block',
            ]); ?>
    </div>
    <div class="col-sm-8">
        <p align="center">
            <b><?= $formatter->asDate($searchModel->date, 'd MMMM y') ?></b>
        </p>
    </div>
    <div class="col-sm-2">
        <?= Html::a(
            '<span class="glyphicon glyphicon-prev"></span> Следующий',
            ['index', 'CalendarSearch' => ['date' => $dateNext->format('Y-m-d')]], [
                'class' => 'btn btn-primary btn-xs btn-block',
            ]); ?>
    </div>
</div>

<table class="table table-bordered" id="cal">
    <thead>
    <tr>
        <th>Пн.</th>
        <th>Вт.</th>
        <th>Ср.</th>
        <th>Чт.</th>
        <th>Пт.</th>
        <th>Сб.</th>
        <th>Вс.</th>
    </tr>
    </thead>
    <tbody>
    <?php while( $date->format('Y-m-d') <= $dateMonthEnd ){ ?>
        <tr>
            <?php for($i = 0; $i < 7; $i++){ ?>
                <?php
                $htmlOptions = [];
                $content = '&nbsp;';
                $format = $date->format('Y-m-d');

                $dayHtmlOptions = ['class' => 'day'];
                if( $format >= $dateMonthBegin && $format <= $dateMonthEnd )
                    $dayHtmlOptions['class'] = $format == $dateCurrent->format('Y-m-d')? 'current-day' : 'month-day';

                $content = '<div>' . Html::a(
                        Yii::$app->getFormatter()->asDate($format, 'd MMMM'),
                        ['index', 'CalendarSearch' => ['date' => $format]],
                        $dayHtmlOptions
                    ) . '</div>';

                $htmlOptions['class'] = 'active';
                $isExists = Calendar::find()->where(['date' => $format])->exists();

                if( $format < $dateCurrent->format('Y-m-d') ) {
                    if( !$isExists )
                        $htmlOptions['class'] = 'danger';
                    else {
                        $htmlOptions['class'] = 'success';
                    }
                } else {
                    if( $isExists )
                        $htmlOptions['class'] = 'warning';
                }

                echo Html::tag('td', $content, $htmlOptions);
                ?>
                <?php $date->add($interval); } ?>
        </tr>
    <?php } ?>
    </tbody>

</table>