<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => 'copy-day-form',
    'enableAjaxValidation' => true,
    'validationUrl' => ['copy-day-validate']
]); ?>

<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'from', ['options' => ['class' => 'hide']])->hiddenInput(); ?>
        <?= $form->field($model, 'from')->label(false)->textInput(['disabled' => true]); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'to')->label(false)->widget(DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'options' => [
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('to')
            ],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ],
        ]) ?>
    </div>
    <div class="col-sm-4">
        <?= Html::submitButton('Копировать', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>