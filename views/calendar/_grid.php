<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use app\models\Calendar;
use app\models\Task;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataDays yii\data\ActiveDataProvider[] */

$dataProvider = $searchModel->search();
//Task::find()->innerJoinWith('calendars')->all();
?>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить', ['create'], [
            'class' => 'btn btn-success btn-xs',
            'data-action' => 'load'
        ]) ?>
        <?= Html::a('<span class="glyphicon glyphicon-copy"></span> Скопировать', ['copy-day', 'from' => $searchModel->date], [
            'title' => Yii::t('yii', 'Копировать'),
            'data-pjax' => '0',
            'data-action' => 'load',
            'class' => 'btn btn-default btn-xs'
        ]); ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> Удалить', ['remove-day', 'from' => $searchModel->date], [
            'title' => Yii::t('yii', 'Delete'),
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
            'data-pjax' => '0',
            'class' => 'btn btn-default btn-xs'
        ]); ?>
    </p>

<?= GridView::widget([
    'id' => 'calendar-items',
    'dataProvider' => $dataProvider,
    'showHeader' => false,
    'layout' => '{items}',
    'rowOptions' => function (Calendar $model, $key, $index, $grid) {
        return $model->isFulfilled()? ['class' => 'success'] : [];
    },
    'columns' => [

        [
            'class' => DataColumn::className(),
            'value' => function($model){
                    $values = [];
                    foreach(['start_time', 'end_time'] as $name) {
                        if( !$model->$name )
                            continue;
                        $time = explode(':', $model->$name);
                        $values[] = $time[0] . ':' . $time[1];
                    }
                    return join(' - ', $values);
                },
            'options' => [
                'style' => 'width: 110px;'
            ]
        ],

        'target.title',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{fulfilled} {update} {delete}',
            'buttons' => [
                'fulfilled' => function($url, Calendar $model, $key){
                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                        'title' => Yii::t('yii', 'Выполнена'),
                        'data-pjax' => '0',
                        //'data-action' => 'load',
                        'class' => 'btn btn-success btn-xs',
                        'disabled' => $model->isFulfilled()
                    ]);
                },
                'update' => function ($url, Calendar $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                        'data-action' => 'load',
                        'class' => 'btn btn-primary btn-xs'
                    ]);
                },
                'delete' => function ($url, Calendar $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                        'class' => 'btn btn-danger btn-xs'
                    ]);
                },
            ],
            'options' => [
                'style' => 'width: 100px;'
            ],
            'urlCreator' => function($action, $model, $key, $index){
                    return [$action, 'id' => $model->id, 'returnUrl' => Yii::$app->request->url];
                }
        ],
    ],
]); ?>