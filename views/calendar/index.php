<?php

use yii\helpers\Html;
use yii\bootstrap\BootstrapAsset;
use app\models\Calendar;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataDays yii\data\ActiveDataProvider[] */

$this->title = 'Планировщик дел';
$this->params['breadcrumbs'][] = $this->title;
BootstrapAsset::register($this);
?>

<div class="calendar-index">

    <div class="row">
        <div class="col-sm-8">
            <?= $this->render('_calendar', ['searchModel' => $searchModel]); ?>
        </div>
        <div class="col-sm-4">
            <?= $this->render('_grid', ['searchModel' => $searchModel]); ?>
        </div>
    </div>

</div>

<?php $jsScript = <<<JS
var element = $('[data-action=load]');
element.each(function(){
    var obj = $(this);

    obj.popover({
        html: true,
        placement: 'bottom',
        trigger: 'click',
        content: function(){
            var content = $('<div>Загрузка...</div>');
            $.ajax({
                url: obj.attr('href')? obj.attr('href') : obj.attr('data-url')
            }).done(function(data){
                content.html(data);
            });
            return content;
        }
    });

    obj.on('show.bs.popover', function(){
        element.filter(function(){
            return this != obj[0];
        }).popover('hide');
    });

    obj.on('click', function(){
        return false;
    });
});
JS;
$this->registerJs($jsScript);
?>

<style>
    .popover {
        max-width: 700px;
        width: 700px;
    }
    .calendar-index .table > tbody > tr > td{
        padding-top: 5px;
        padding-bottom: 5px;
    }
    #cal tbody td{
        height: 100px;
        width: 14.3%;
    }
    #cal tbody td .current-day{
        text-decoration: underline;
        font-weight: bold;
    }
    #cal tbody td .day {
        color: silver;
    }
</style>


