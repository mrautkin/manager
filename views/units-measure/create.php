<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitsMeasure */

$this->title = 'Создание единицы измерения';
$this->params['breadcrumbs'][] = ['label' => 'Единицы измерения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="units-measure-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
